class CategoriesController < ApplicationController
  def index
        @categories = Category.all
  end

  def new
  end

  def create
    @category = Category.new(category_params)
  end

  def update
  end

  def show
    @category = Category.find(params[:id])
    @title = @category.cname
    @posts = @category.posts
  end
  
  private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_categor
    @category = Category.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def category_params
    params.require(:category).permit(:category_id, :cname)
  end

end
