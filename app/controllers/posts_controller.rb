class PostsController < ApplicationController

helper_method :sort_column, :sort_direction

  def index
    @q = Post.ransack(params[:q])
    @posts = @q.result.page(params[:page]).per(10)
  end

  def show
    @post = Post.find(params[:id])
    @user = User.all
    @post_comment = PostComment.new(:post => @post)
  end

  def new
    @post = Post.new
  end

  def edit
    @post = Post.find(params[:id])
  end

  def create
    @post = Post.new(post_params)
  end

  def update
  end

  def destroy
    @post.destroy
  end
  
  private
  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @post = Post.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def post_params
    params.require(:post).permit(:title, :body, :category_id, :user_id, :admin_user_id, :email)
  end
end