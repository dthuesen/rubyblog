class Post < ActiveRecord::Base
    has_paper_trail
    belongs_to :category
    belongs_to :user
    has_many :post_comments, :dependent => :destroy
    validates :title, presence: true
    validates :body, length: { minimum: 10 }
    
    def to_s
    "#{title}"
    end
    
end
