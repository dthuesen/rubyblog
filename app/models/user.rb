class User < ActiveRecord::Base
    has_paper_trail
    devise :database_authenticatable, 
          :recoverable, :rememberable, :trackable, :validatable
    before_save { self.email = email.downcase }
    validates :name,    presence: true, length: {minimum: 4, maximum:  50 }
    validates :username,    presence: true, length: {minimum: 4, maximum:  50 }
    VALID_EMAIL_REGEX = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
    validates :email,   presence: true, length: { maximum: 255 },
                        format: { with: VALID_EMAIL_REGEX },
                        uniqueness: { case_sensitive: false }
    # has_secure_password
    validates :password, presence: true, length: { minimum: 6 }
    
    has_many :posts

end
