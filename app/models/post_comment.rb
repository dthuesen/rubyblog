class PostComment < ActiveRecord::Base
  belongs_to :post
  
  validates_presence_of :name
  validates_length_of :name, :within => 2..20
  validates_presence_of :body
  validates_length_of :body, minimum: 10
end
