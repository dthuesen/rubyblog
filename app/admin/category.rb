ActiveAdmin.register Category do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
permit_params :cname

    index do
        column :id
        column "Category", :cname
        column "Author", :email
        
        actions
    end

  preserve_default_filters!
  filter :posts, as: :select, label: 'Posts in da house'
  filter :cname, as: :string, label: 'Category Name'
  filter :email, as: :string, label: 'Email'


# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


end