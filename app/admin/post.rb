ActiveAdmin.register Post do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
 permit_params :title, :body, :category_id, :user_id, :admin_user_id, :email, :name, :updated_at, :created_at
# or

  index do
    selectable_column
    column :id
    column "Post Title", :title
    column "Post Text", :body
    column 'Author', :email do |post|
      link_to post.user.name, [:admin, post]
    end
    # column "Email", :username
    actions 
    
  end

  show :title => :title

  # show do
  #   # h3 post.title
  #   div :class =>"postshowbody" do
  #     simple_format post.body
  #   end
    
  #   div do
  #     simple_format post.email
  #   end
    
  #   div do
  #     simple_format post.created_at
  #   end
  # end


end
