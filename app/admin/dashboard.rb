ActiveAdmin.register_page "Dashboard" do
    content :title => proc{ I18n.t("active_admin.dashboard") } do
      columns do
        column do
          panel "All Posts" do
            table_for Post.order("updated_at desc").limit(5) do
              column :id, selectable: true
              column 'Title', :name do |post|
                link_to post.title, [:admin, post]
              end
              column 'Author', :email do |post|
                link_to post.user.name, [:admin, post]
              end
              column 'Category', :category.to_s, :sortable => :cname
              column 'Last edited at', :updated_at 
            end
            strong { link_to "View All Posts", admin_posts_path }
          end
        end
      end
      
      columns do
        column do
          panel "All Categories" do
            table_for Category.order("updated_at desc").limit(5) do
              column :id, selectable: true
              column :name do |category|
                link_to category.cname, [:admin, category]
              end
              column 'Posts', :post.to_s
              column 'Last edited at', :updated_at 
              column 'Author', :email
            end
            strong { link_to "View All Categories", admin_categories_path }
          end
        end
      end

    end
  
end
