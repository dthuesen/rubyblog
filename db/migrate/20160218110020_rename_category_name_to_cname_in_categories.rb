class RenameCategoryNameToCnameInCategories < ActiveRecord::Migration
  def change
    rename_column :categories, :category_name, :cname
  end
end
