
task :fakeuser => :environment do
        User.create(name: Faker::Name.name,
                    email: Faker::Internet.safe_email, 
                    username: Faker::Internet.user_name, 
                    password:'password', 
                    password_confirmation: 'password')
end