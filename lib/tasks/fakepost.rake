
task :fakepost => :environment do
    50.times do
        Post.create(title: Faker::Book.title,
                    body: Faker::Lorem.paragraphs(2, true),
                    category_id: Faker::Number.between(10, 17), 
                    user_id: Faker::Number.between(1, 101), 
                    admin_user_id: Faker::Number.between(1, 1), 
                    email: Faker::Internet.safe_email)
    end
end