# README #


### What is this repository for? ###

Learning repo
Built with:

* Ruby 2.2.2p95 (2015-04-13 revision 50295) [x86_64-linux]
* Rails 4.2.5.1

### Comments to this repo ###

Because the curse with wich I built this app was outdated - Rails 3.0 in 
in the tutorial on udemy - I had to do a lot research to get things run.
This tutorial was not TDD instructed so the tests mostly aren't done well.

This app is a blog (what else? Every tutorial out there seams to take blogs 
for teaching (few don't). It has as features:

* ActiveAdmin as BackEnd
* devise ~> 3.2 gem for authentication
* Using the faker gem fakepost.rake for filling the dev db with huge amount
of fake posts and fakeuserrake for filling with some fake users.
* Partials for sidebar, header and post_comments were used in views.
* Commenting functionality was added as well.
* Some static pages were built for just building them. 
* Some small features took place like: pagination, date
(post.updated_at.strftime("%b %d, %Y")), back-link, 
category-click-through and a truncation to long blog posts in
home#index view (<%= truncate post.body, length: 250 %>)

### Who do I talk to? ###

* Repo owner: me
* Other community or team contact? No.